@echo off
title Windows 10 Varaus kuvake korjaus
goto foreward
:foreward
color 0b
cls
echo Metodit t�ss� bat tiedostossa muokkaavat tiedostoja ja rekisteri�.
echo N�m� ovat testattu toimivaksi, mutta Min� en ota mit��n vastuuta t�m�n tiedoston k�yt�st�.
echo T�ll� tiedostolla ei ole mit��n takuita. Kaikki vahingot ovat sinun omalla vastuulla.
echo.
echo Voi olla ett� jotkut Virusohjelmat liputtavat .bat tiedostoja. Jos ep�ilet t�m�n aitoutta, ole hyv� ja lue koodi l�pi.
echo.
echo Jos ymm�rr�t n�m� ehdot ja hyv�ksyt ne - paina mit� tahansa n�pp�int� jatkaaksesi...
pause > NUL
goto elevatecheck
:elevatecheck
color 0c
cls
echo Tarkistetaan J�rjestelm�nvalvojan oikeuksia...
echo.
echo.
openfiles > NUL 2>&1
if %errorlevel%==0 (
echo Oikeudet l�yty, jatketaan...
goto vercheck
) else (
echo Et ole J�rjestelm�nvalvoja...
echo T�m� bat tiedosto ei voi tehd� teht�v��ns� ilman oikeuksia!
echo.
echo Paina oikeaa korvaa ja valitse ^'Suorita J�rjestelm�nvalvojana^' ja yrit� uudestaan...
echo.
echo Paina mit� napp�int� poistuaksesi...
pause > NUL
exit
)
:vercheck
color 0c
cls
echo Aloitetaan tarkistuksia...
for /f "tokens=4-5 delims=. " %%i in ('ver') do set version=%%i.%%j
if "%version%"=="6.3" set allow=1
if "%version%"=="6.1" set allow=1
if %allow%==1 goto warning
set allow=0
echo.
echo Et l�p�issyt vaadittavia edellytyksi�.
echo Jos sinulla on Windows 8, asenna Windows 8.1 p�ivitys storesta.
echo.
echo Paina mit� tahansa n�pp�int� poistuaksesi.
pause > NUL
exit
:warning
color 0b
cls
echo Varoitus p�tevyyksist�...
echo.
echo.
echo Vaikka j�rjestelm�si ei ole p�ivitys kelpoinen, ei tarkoita ett� olisit kelpoinen ilmaiseen p�ivitykseen.
echo Ainakin n�m� versiot EIV�T ole kelpoisia kyseiseen p�ivitykseen Windows Update kautta...
echo.
echo Windows XP
echo Windows Vista
echo Windows 7 RTM
echo Windows 8
echo Windows 8.1 RTM
echo Windows 7/8/8.1 Enterprise
echo Windows RT (Surface RT ja Surface 2)
echo Windows Phone 8.0
echo.
echo Paina mit� n�pp�int� jatkaaksesi...
pause > NUL
goto menu
:menu
color 0b
cls
echo P��valikko
echo.
echo.
echo 1^) Tarkista p�ivitysten tila
echo 2^) Nopea metodi #1
echo 3^) Nopea metodi #2
echo 4^) Pitk� metodi #1
echo 5^) POISTU
echo.
set /p mmchoice=Selection: 
if %mmchoice%==1 goto upstatus
if %mmchoice%==2 goto qm1
if %mmchoice%==3 goto qm2
if %mmchoice%==4 goto lm1
if %mmchoice%==5 exit
goto error
:error
color 0C
cls
echo P��valikko - VIRHE!
echo.
echo.
echo Et sy�tt�nyt validia numeroa.
echo.
echo Paina mit� tahansa n�pp�int� palataksesi p��valikkoon ja yrit� uudelleen.
pause > NUL
goto menu
:upstatus
cls
echo Tarkistetaan p�ivitysten asennus tilaa...
echo.
echo.
if "%version%"=="6.3" goto upstatus8
if "%version%"=="6.1" goto upstatus7
goto menu
:upstatus8
echo Windows 8^+ l�ydetty...
echo.
set upcheck=3035583
echo Etsit��n p�ivityst� KB%upcheck%...
dism /online /get-packages | findstr %upcheck% > NUL
if %errorlevel%==0 (
echo P�ivitys KB%upcheck% on asennettu!
set missupdate=0
) else (
echo P�ivitys KB%upcheck% puuttuu!
set missupdate=1
)
echo.
set upcheck=3035583
echo Tarkistetaan p�ivityst� KB%upcheck%...
dism /online /get-packages | findstr %upcheck% > NUL
if %errorlevel%==0 (
echo P�ivitys KB%upcheck% on asennettu!
) else (
echo P�ivitys KB%upcheck% puuttuu!
set /a missupdate=%missupdate%+1>NUL
)
echo.
echo.
if %missupdate%==0 (
echo Sinulta ei puutu mit��n p�ivityksi�, onnittelut!
) else (
echo Sinulta puuttuu %missupdate% paketti^(t^).
)
echo Paina mit� tahansa n�pp�int� palataksesi p��valikkoon...
pause > NUL
goto menu
:upstatus7
echo Windows 7 l�ydetty...
echo.
set upcheck=3035583
echo Etsit��n p�ivityst� KB%upcheck%...
dism /online /get-packages | findstr %upcheck% > NUL
if %errorlevel%==0 (
echo P�ivitys KB%upcheck% is asennettu!
set missupdate=0
) else (
echo P�ivitys KB%upcheck% puuttuu!
set missupdate=1
)
echo.
set upcheck=2952664
echo Etsit��n p�ivityst� KB%upcheck%...
dism /online /get-packages | findstr %upcheck% > NUL
if %errorlevel%==0 (
echo P�ivitys KB%upcheck% is asennettu!
) else (
echo P�ivitys KB%upcheck% puuttuu!
set /a missupdate=%missupdate%+1>NUL
)
echo.
echo.
if %missupdate%==0 (
echo Sinulta ei puutu mit��n p�ivityksi�, onnittelut!
) else (
echo Sinulta puuttuu %missupdate% paketti^(t^).
)
echo Paina mit� tahansa n�pp�int� palataksesi p��valikkoon...
pause > NUL
goto menu
:qm1
cls
echo Nopea metodi #1
echo.
echo.
echo P�ivitet��n rekisteri�...
reg add "HKLM\Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\UpgradeExperienceIndicators" /v UpgEx /t REG_SZ /d Green /f
echo Kokeillaan k�ynnist�� ilmoitusta ...
%SystemRoot%\System32\GWX\GWX.exe /taskLaunch
echo.
echo T�m� metodi on nyt valmis, ja my�s nopea
echo Sinun nyt pit�isi n�hd� Windows 10 ikoni ilmoitus alueella.
echo Jos ei, palaa takaisin p��valikkoon ja koikeile toista vaihtoehtoa.
echo.
echo Paina mit� tahansa n�pp�int� palataksesi p��valikkoon...
pause > NUL
goto menu
:qm2
echo Nopea metodi #2
echo.
echo.
echo Yritet��n k�ynnist�� GWX tehte�v��...
%SystemRoot%\System32\GWX\GWX.exe /taskLaunch
echo P�ivitet��n GWX konfigurointi...
%SystemRoot%\System32\GWX\GWXConfigManager.exe /RefreshConfig
echo.
echo T�m� metodi on nyt valmis, mutta siin� voi menn� muutama minuutti.
echo Noin 10 minuutin p��st� sinun pit�isi n�hd� Windows 10 ikoni ilmoitus alueella.
echo Jos ei, palaa takaisin p��valikkoon ja koikeile toista vaihtoehtoa.
echo.
echo Paina mit� tahansa n�pp�int� palataksesi p��valikkoon...
pause > NUL
goto menu
:lm1
cls
echo Pitk� metodi #1
echo.
echo.
echo T�m� metodi voi kest�� jopa 10 minuuttia tai enemm�n . 
echo Minulla t�m� ajoi l�helt� 40 minuuttia kun testasin t�t�...
echo.
echo T�m� voi looppaa jonkin aikaa muuta huomioi t�m�...
echo Jos n�et ^"STATUS^" mutta et n�e ^"RUNNING^", jotain on pieless'.
echo Jos t�m� tapahtuu, sulje bat tiedosto ja aloita alusta. Sen voi juotua tekem��n pari kertaa.
echo.
echo Ja koska miten t�m� scripti on kirjoitettu, sinun pit�� uudestaan k�ynnist�� t�m� bat tiedosto.
echo.
echo Jos ymm�rr�t t�m�n, paina mit� tahansa napp�int� jatkaaksesi.
pause > NUL
color 0c
cls
echo Ty� on alkanut...
REG QUERY "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\UpgradeExperienceIndicators" /v UpgEx | findstr UpgEx
if "%errorlevel%"=="0" goto RunGWX
reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Appraiser" /v UtcOnetimeSend /t REG_DWORD /d 1 /f
schtasks /run /TN "\Microsoft\Windows\Application Experience\Microsoft Compatibility Appraiser"
echo T�M� VOI LUUPPAA - TARKISTA RUNNING TILANNE!!! &echo T�M� VOI LUUPPAA - TARKISTA RUNNING TILANNE!!! &echo T�M� VOI LUUPPAA - TARKISTA RUNNING TILANNE!!!
:CompatCheckRunning
schtasks /query /TN "\Microsoft\Windows\Application Experience\Microsoft Compatibility Appraiser"
schtasks /query /TN "\Microsoft\Windows\Application Experience\Microsoft Compatibility Appraiser" | findstr Ready
if not "%errorlevel%"=="0" ping localhost > NUL &goto :CompatCheckRunning
:RunGWX
schtasks /run /TN "\Microsoft\Windows\Setup\gwx\refreshgwxconfig"
color 0b
cls
echo.
echo T�m� metodi on nyt valmis
echo Raportit kertovat ett� voi menn� jopa tunti ett� ikoni n�kyy.
echo Jos kuvaketta ei n�y noin tunnin p��st�, k�ynnist� kone uudestaan.
echo.
echo Jos t�m� metodi ei toiminut, kokeile toista metodia.
echo Jos teit t�m�n viimeisen�, ota yhteytt� Micorsoft Answers foorumille jotta saat apua.
echo.
echo Paina mit� tahansa n�pp�int� palataksesi p��valikkoon...
pause > NUL
goto menu